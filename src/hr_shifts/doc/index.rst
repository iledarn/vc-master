============================
My First Module Documentation
============================


This module allows you to <[...]>


Use Case
========

<[Short paragraf about the use case.]>

Installation
============

* Install this module in the usual way

Configuration
=============

1. <[Step]>
2. <[Step]>
3. <[Step]>

Example Workflow
================

<[Walk through the different usage scenarios of this module.]>

Implementation Notes
====================

No notes available, yet.

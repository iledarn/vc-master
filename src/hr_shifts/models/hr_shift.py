from datetime import timedelta

from odoo import _, api, fields, models  # noqa


class ShiftArea(models.Model):
    _name = "hr.shift.area"

    name = fields.Char(string="Area", required=True)
    parent_id = fields.Many2one("hr.shift.area", string="Parent Area", index=True)


class Shift(models.Model):
    _name = "hr.shift"

    name = fields.Char()

    area_id = fields.Many2one("hr.shift.area", string="Area")
    employee_ids = fields.Many2many("hr.employee", string="Employees")
    start = fields.Datetime("From", required=True)
    stop = fields.Datetime("To", required=True)
    duration = fields.Float("Duration")

    @api.onchange("start", "duration")
    def _onchange_duration(self):
        if self.start:
            self.stop = (
                self.start + timedelta(hours=self.duration) - timedelta(seconds=1)
            )


class ShiftEmployeeFilter(models.Model):
    _name = "hr.shift.employee.filter"

    employee_id = fields.Many2one("hr.employee", required=True)
    user_id = fields.Many2one(
        "res.users", "Me", required=True, default=lambda self: self.env.user
    )
    active = fields.Boolean("Active", default=True)

odoo.define('hr_shifts.shift_calendar', function(require) {
    'use strict';

    var core = require('web.core');
    var CalendarController = require("web.CalendarController");
    var time = require('web.time');
    var CalendarModel = require('web.CalendarModel');
    var CalendarRenderer = require('web.CalendarRenderer');
    var CalendarView = require('web.CalendarView');
    var viewRegistry = require('web.view_registry');
    var _t = core._t;

    var ShiftCalendarController = CalendarController.extend({
    });

    var ShiftCalendarModel = CalendarModel.extend({
         /**
          * Display everybody's shifts if no employee filter exists
          * @private
          * @override
          * @param {any} filter
          * @returns {Deferred}
         */
        // _loadFilter: function (filter) {
        //     return this._super.apply(this, arguments).then(function () {
        //         var filters = filter.filters;
        //         var all_filter = filters[filters.length - 1];

        //         if (all_filter) {
        //             all_filter.label = _t("Everybody's shifts");

        //             if (filter.write_model && filter.filters.length <= 1 && all_filter.active === undefined) {
        //                 filter.all = true;
        //                 all_filter.active = true;
        //             }
        //         }

        //     });
        // }
    });

    var ShiftCalendarView = CalendarView.extend({
        config: _.extend({}, CalendarView.prototype.config, {
            Controller: ShiftCalendarController,
            Model: ShiftCalendarModel,
            Renderer: CalendarRenderer,
        }),
    });

    viewRegistry.add('shifts_calendar', ShiftCalendarView);

});
